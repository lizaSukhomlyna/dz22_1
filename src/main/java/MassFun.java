import java.util.Arrays;

public class MassFun {
    public static int[] getArrayAfterFour(int[] mas) {
        int[] indexFour = new int[mas.length];
        int j = 0;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == 4) {
                indexFour[j] = i;
                j++;
            }
        }
        int maxFourIndex = Arrays.stream(indexFour)
                .max()
                .getAsInt();

        int[] afterFour = new int[mas.length - maxFourIndex - 1];
        int k = 0;
        for (int i = maxFourIndex + 1; i < mas.length; i++) {
            afterFour[k] = mas[i];
            k++;
        }
        return afterFour;


    }

    public static boolean consistOfOneAndFour(int[] mas) {
        long numOfAnotherNumbers = Arrays.stream(mas)
                .filter(el -> el != 1 && el != 4)
                .count();
        return Arrays.stream(mas).anyMatch(s -> s == 1)
                && Arrays.stream(mas).anyMatch(s -> s == 4)
                && numOfAnotherNumbers == 0;
    }

}
