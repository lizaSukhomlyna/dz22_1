import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MassFunTest {

    @Test
    void getArrayAfterFour() {
        Assert.assertArrayEquals(new int[]{5, 5, 6, 7}, MassFun.getArrayAfterFour(new int[]{1, 2, 4, 12, 11, 4, 1, 2, 3, 4, 5, 5, 6, 7}));
        Assert.assertArrayEquals(new int[]{1}, MassFun.getArrayAfterFour(new int[]{4, 4, 4, 4, 4, 4, 4, 1}));
        Assert.assertArrayEquals(new int[0], MassFun.getArrayAfterFour(new int[]{1, 1, 1, 1, 1, 1, 1, 4}));
        Assert.assertArrayEquals(new int[]{3, 3, 3}, MassFun.getArrayAfterFour(new int[]{4, 3, 3, 3}));
    }

    @Test
    void consistOfOneAndFour() {
        assertTrue(MassFun.consistOfOneAndFour(new int[]{1,1,1,4,1}));
        assertTrue(MassFun.consistOfOneAndFour(new int[]{4,4,4,1}));
        assertFalse(MassFun.consistOfOneAndFour(new int[]{4,4,4,1,3}));
        assertFalse(MassFun.consistOfOneAndFour(new int[]{1,1,1,1,1}));
        assertFalse(MassFun.consistOfOneAndFour(new int[]{4,4,4,4,4}));
    }
}